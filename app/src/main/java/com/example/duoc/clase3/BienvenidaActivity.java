package com.example.duoc.clase3;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.net.URI;


public class BienvenidaActivity extends ActionBarActivity {

    private Button btnLlamar, btnAbrirURL, btnEnviarMail, btnCalculadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenida);
        btnLlamar = (Button) findViewById(R.id.btnLlamar);
        btnAbrirURL = (Button) findViewById(R.id.btnAbrirURL);
        btnEnviarMail = (Button) findViewById(R.id.btnEnviarMail);
        btnCalculadora = (Button) findViewById(R.id.btnCalculadora);

        btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickLlamar();
            }
        });

        btnAbrirURL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAbrirURL();
            }
        });

        btnEnviarMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickEnviarMail();
            }
        });

        btnCalculadora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickCalculadora();
            }
        });
    }

    private void onClickCalculadora(){
        Intent i = new Intent(this, CalculadoraActivity.class);
        startActivity(i);
    }

    private void onClickEnviarMail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "abc@gmail.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    private void onClickAbrirURL() {
        String url = "http://www.duoc.cl";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }



    private void onClickLlamar() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel: 555555500"));
        startActivity(callIntent);
    }


}

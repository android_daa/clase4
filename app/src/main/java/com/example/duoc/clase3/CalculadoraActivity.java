package com.example.duoc.clase3;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class CalculadoraActivity extends AppCompatActivity {

    private TextView txtResultado;
    private EditText txtNum1, txtNum2;
    private Button btnSumar, btnRestar, btnDividir, btnMultiplicar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        txtNum1 = (EditText)findViewById(R.id.txtNum1);
        txtNum2 = (EditText)findViewById(R.id.txtNum2);

        txtResultado = (TextView)findViewById(R.id.txtResultado);
        btnSumar = (Button)findViewById(R.id.btnSumar);
        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int num1 = Integer.parseInt(txtNum1.getText().toString());
                int num2 = Integer.parseInt(txtNum2.getText().toString());
                int resultado = num1+num2;
                txtResultado.setText(resultado+"");

            }
        });
    }


}
